const prompt = require('prompt-sync')();

function factorial(n){
    if(n >= 1){
        return n * factorial(n-1);
    }else{
        return 1;
    }
}

function formula_expansion(y,n){
    let finalResult = "";
    let nFactorial = factorial(n)

    for(var i = 0; i < n+1; i++){
        let nMinusOneFact = factorial(n-i)
        let rFact = factorial(i)

        let ncr = nFactorial / (nMinusOneFact * rFact) 
        let xPow = (n-i)
        let yPow = Math.pow(y, i);
        let value = ncr * yPow;

        if(i == n){
            if(value == 1){
                if(xPow == 0){
                    finalResult += `1`
                }else if(xPow == 1){
                    finalResult += `x`
                }else{
                    finalResult += `x^${xPow}`
                } 
            }else{
                if(xPow == 0){
                    finalResult += `${value}`
                }else if(xPow == 1){
                    finalResult += `${value}x` 
                }else{
                    finalResult += `${value}x^${xPow}`
                }
            }
        }else{
            if(value == 1){
                if(xPow == 0 || xPow == 1){
                    finalResult += `x + `
                }else{
                    finalResult += `x^${xPow} + `
                } 
            }else{
                if(xPow == 0 || xPow == 1){
                    finalResult += `${value}x + `
                }else{
                    finalResult += `${value}x^${xPow} + ` 
                } 
            }
        }
    }
    return finalResult;
}

function start(){
    let y = 1;
    let n = prompt('Enter n value in the range of 0 - 50 :  ');
    n = parseInt(n)
    if(n >= 0 && n <= 50){
        let result = formula_expansion(y,n)
        console.log(result)
    }else{
        console.log("n value out of range")
    }
}

start()